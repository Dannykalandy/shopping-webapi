﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace shopping_webAPI.Models
{
    public class usercontext
    {
        public string ConnectionString { get; set; }

        public usercontext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public List<user> GetAlluser()
        {
            List<user> list = new List<user>();

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM user", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new user()
                        {
                            id = reader.GetInt32("id"),
                            username = reader.GetString("username"),
                            password = reader.GetString("password"),
                            email = reader.GetString("email"),
                            phone = reader.GetString("phone"),
                            country = reader.GetString("country"),
                            city = reader.GetString("city"),
                            postcode = reader.GetString("postcode"),
                            name = reader.GetString("name"),
                            address = reader.GetString("address")
                        });
                    }
                }
            }
            return list;
        }

        public List<user> Getuser(string id)
        {
            List<user> list = new List<user>();

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM user WHERE id = @id", conn);
                cmd.Parameters.AddWithValue("@id", id);

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new user()
                        {
                            id = reader.GetInt32("id"),
                            username = reader.GetString("username"),
                            password = reader.GetString("password"),
                            email = reader.GetString("email"),
                            phone = reader.GetString("phone"),
                            country = reader.GetString("country"),
                            city = reader.GetString("city"),
                            postcode = reader.GetString("postcode"),
                            name = reader.GetString("name"),
                            address = reader.GetString("address")
                        });
                    }
                }
            }
            return list;
        }
    }
}
