﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace shopping_webAPI.Models
{
    public class shoppingcontext
    {
        public string ConnectionString { get; set; }

        public shoppingcontext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public List<shopping> GetAllshopping()
        {
            List<shopping> list = new List<shopping>();

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM shopping", conn);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new shopping()
                        {
                            id = reader.GetInt32("id"),
                            nama = reader.GetString("nama"),
                            createdate = reader.GetString("createdate")
                        });
                    }
                }
            }
            return list;
        }

        public List<shopping> Getshopping(string id)
        {
            List<shopping> list = new List<shopping>();

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM shopping WHERE id = @id", conn);
                cmd.Parameters.AddWithValue("@id", id);

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new shopping()
                        {
                            id = reader.GetInt32("id"),
                            nama = reader.GetString("nama"),
                            createdate = reader.GetString("createdate")
                        });
                    }
                }
            }
            return list;
        }
    }
}
